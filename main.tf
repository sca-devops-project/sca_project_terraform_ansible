terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }

  backend "s3" {
    bucket = "sca-terra-ansible-bucket"
    key = "tf-statefile"
    region = "us-east-2"
    
  }

}


# Configure the AWS Provider 
provider "aws" {
  region = "us-east-2"
  profile = "default"
}


resource "aws_key_pair" "sca-project-pub-key" {
  key_name = "sca-project-pub-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCuGP2Z04x+uhEyzuBe7PSD4Y3fA16tvms1xoke7CqKjNRmXH5bcE6OnxPFqMsi0/h0lSu+G6W8nl3OBR/MwhRF+AjkP97T9juciT2y5MDYcPS8gh7ps+egOCIr+YKvMmtV99f74LQ9mHIpWwj0q41okzC7CfnMnSULc0EBYBHntQlwKmNZUZ+QRZi94TRGSEVkGRx7ajv+cJoWeX4Ugfa09nRcqGZoHf4AKt9n4fbhisRHaBjTmCntiwcje6/j9EkSboi/acxLeRy0GnVfNG3gFjIIyMwPGucnVjiTt8+f808QLELLx7RAAnkceI7kYeEERMgKKRWYHGsLYDv3EAVN Mawhizzle@Macs-MacBook-Pro-8.local"
  
}

#create ubuntu server 
resource "aws_instance" "sca-project-webserver" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.micro"
  availability_zone = "us-east-2a"
  count = 1
  key_name = aws_key_pair.sca-project-pub-key.key_name

  tags = {
    Name = "sca_app_server"
  }

}

# Create a new load balancer
resource "aws_elb" "sca-project-elb" {
  name               = "sca-project-elb"
  instances = aws_instance.sca-project-webserver.*.id
  availability_zones = ["us-east-2a", "us-east-2b", "us-east-2c"]

  listener {
    instance_port     = 8000
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  tags = {
    Name = "sca-project-elb"
  }
}

output "instance_ips" {
  value = aws_instance.sca-project-webserver.*.public_ip
  
}
