 FROM ubuntu:latest

 RUN apt-get update -yqq && apt-get install -y gnupg software-properties-common curl python-apt unzip jq

 RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -

 RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

 RUN apt-get update && apt-get install terraform

 RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

 RUN unzip awscliv2.zip

 RUN ./aws/install